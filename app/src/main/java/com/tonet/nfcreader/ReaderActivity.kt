package com.tonet.nfcreader

import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_reader.*
import java.lang.RuntimeException

class ReaderActivity : AppCompatActivity() {

    private var nfcAdapter: NfcAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader)

        supportActionBar?.hide()

        bnavig.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nfcReaderPage -> {
                    startActivity(Intent(this, ReaderActivity::class.java))
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.nfcWriterPage -> {
                    startActivity(Intent(this, WriterActivity::class.java))
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }
        }
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (nfcAdapter == null) {
            showNfcAlert()
        }
        if (!(nfcAdapter!!.isEnabled)) {
            Toast.makeText(this, "Please, enable NFC in settings", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showNfcAlert() {
        val builder = AlertDialog.Builder(this)
            .setPositiveButton("OK") { _, _ ->
                finish()
            }
            .setCancelable(false)
        val alert = builder.create()
        alert.setTitle("NFC unavailable")
        alert.setMessage("Check your NFC module")
        alert.setIcon(R.drawable.ic_launcher)
        alert.show()
    }

    override fun onNewIntent(intent: Intent) {

        receiveMessageFromDevice(intent)

        super.onNewIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bottom_navigation_menu, menu)
        return true
    }

    override fun onResume() {
        super.onResume()

        enableForegroundDispatchers(this, nfcAdapter!!)
        receiveMessageFromDevice(intent)
    }

    override fun onPause() {
        super.onPause()

        disableForegroundDispatchers(this, nfcAdapter!!)
    }

    private fun receiveMessageFromDevice(intent: Intent) {
        val action = intent.action
        if (NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
            val parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            with(parcelables) {
                val inDefMessage = this?.get(0) as NdefMessage
                val inDefRecords = inDefMessage.records
                val ndefRecord_0 = inDefRecords[0]

                val inMessage = String(ndefRecord_0.payload)
                tagValue.text = inMessage
            }
        }
    }

    private fun enableForegroundDispatchers(activity: AppCompatActivity, adapter: NfcAdapter) {
        val intent = Intent(activity.applicationContext, activity.javaClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(activity.applicationContext, 0, intent, 0)
        val filters = arrayOfNulls<IntentFilter>(1)
        val techList = arrayOf<Array<String>>()
        filters[0] = IntentFilter()
        with(filters[0]) {
            this?.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED)
            this?.addCategory(Intent.CATEGORY_DEFAULT)
            try {
                this?.addDataType("text/plain")
            } catch (ex: Exception) {
                throw RuntimeException("Check your mime type")
            }
        }
        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList)
    }

    private fun disableForegroundDispatchers(activity: AppCompatActivity, adapter: NfcAdapter) {
        adapter.disableForegroundDispatch(activity)
    }
}